#!/usr/bin/env python
# coding: utf-8

import os
import sys
import time
import json
import random
from datetime import datetime

import paramiko
import psycopg2
import pandas as pd

from stanc_db_const import POSTGRES_SOURCE_TABLE as pg_cred, EDMP_FTP_HOST, EDMP_FTP_USERNAME, EDMP_FTP_PORT, \
    EDMP_FTP_PRIVATE_KEY_FILE_PATH, EDMP_FTP_REMOTE_FILE_PATH
# from stanc_constants import POSTGRES_SOURCE_TABLE as pg_cred
from stanc_constants import BIL_JSON_FIELDS, BIL_PROC_VARS, BIL_PORTAL_COLUMNS, \
    shareholderDetailsTableHeading_details, litigationCompany_details, litigationIndividualData_details, \
    financialCommitments_user_bil_details, guarantorMyInfoData_details, guarantorDetails_details, \
    Offs_Director_System_bil_details, etbNtb_icm_system_details, relatedPartyRiskCode_details, guarantorRiskCodes_details, \
    companyRiskCode_details
from helpers import send_attachment_email

# this file contains the EDMP BIL's columns and its corresponding business names.
total_edmp_columns = pd.read_csv("/scb-etl/edmp/total_edmp_bil_columns_mappings.csv")

# this file contains mappings for key_id, action_key and its respected business action name.
EDMP_BIL_ACTION_MAP = pd.read_csv("/scb-etl/edmp/edmp_bil_action-role-mappings.csv")

INITIAL_TIME = "2020-08-11 00:00:00.000"
END_TIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

sleep_time = random.randint(1, 100)
print(f"sleep time(in seconds): {sleep_time}")
time.sleep(sleep_time)

# etl_logs table is a monitoring kind of table. Which tracks when was the last time this script was executed and if it was a success/failure.
# create etl_logs table if it doesn't exist.
CREATE_ETL_LOGS_SQL = """
           CREATE TABLE if not exists sc_edmp_etl_logs(
           id serial PRIMARY KEY,
           start_time timestamp NOT NULL,
           end_time timestamp NOT NULL,
           processed_date timestamp NOT NULL DEFAULT NOW(),
           status text);
        """

# get the latest entry from etl_logs table.
SELECT_SQL = "select processed_date from sc_edmp_etl_logs where processed_date > NOW() - INTERVAL '1 HOURS';"

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']} options='-c search_path={pg_cred['schema']}'")
    pg_cur = pg_conn.cursor()
    pg_cur.execute(CREATE_ETL_LOGS_SQL)
    pg_cur.execute(SELECT_SQL)
    pg_conn.commit()
    records = pg_cur.fetchall()
    pg_conn.close()
except Exception as e:
    print(e)
    pg_conn.close()
    sys.exit(1)
if len(records) > 0:
    print("Script is already executed from parallel server... exiting from here")
    sys.exit(0)

"""When the first time script runs, etl_logs doesn't exist and so we put INITIAL_TIME as a start_time.
    In all other cases, start_time is previous end_time and end_time is current time of the script.
"""
insert_time_sql = f"""
        INSERT into sc_edmp_etl_logs (start_time, end_time, processed_date, status)
        (SELECT
            CASE
                WHEN MAX(end_time) is null THEN '{INITIAL_TIME}'
                ELSE MAX(end_time)
            END,
            '{END_TIME}', '{END_TIME}', 'Processing'
        FROM  sc_edmp_etl_logs);
"""

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']} options='-c search_path={pg_cred['schema']}'")
    pg_cur = pg_conn.cursor()
    pg_cur.execute(insert_time_sql)
    pg_conn.commit()

    GET_LATEST_ENTRY_IN_ETL_SQL = f"""select id as ID , start_time as START_TIME, status as STATUS from sc_edmp_etl_logs order by id desc limit 1"""
    pg_cur.execute(GET_LATEST_ENTRY_IN_ETL_SQL)
    MAX_ID, START_TIME, STATUS = pg_cur.fetchone()
    START_TIME_STR = START_TIME.strftime("%Y-%m-%d %H:%M:%S")
    pg_conn.close()
except Exception as e:
    print(e)
    pg_conn.close()
    sys.exit(1)

print(f"Script will fetch data from DB for time duration: {START_TIME_STR} TO {END_TIME}")

print(f"Script Execution Time: {END_TIME}")

# get all proc_inst_id_ where approvalStatus='Rejected' OR isBilRejected=true within time duration of START_TIME_STR and END_TIME.
GET_REJECTED_PROC_IDS_SQL = f"""
SELECT DISTINCT proc_inst_id_
FROM
    act_hi_varinst
WHERE
    name_ in ('approvalStatus', 'isBilRejected')
    AND
    text_ in ('true', 'Rejected')
    AND
    last_updated_time_ > '{START_TIME_STR}'
    AND
    last_updated_time_ <= '{END_TIME}'
"""

# get all scope_id_ where approvalStatus='Rejected' OR isBilRejected=true within time duration of START_TIME_STR and END_TIME.
GET_REJECTED_SCOPE_IDS_SQL = f"""
SELECT DISTINCT scope_id_
FROM
    act_hi_varinst
WHERE
    name_ in ('approvalStatus', 'isBilRejected')
    AND
    text_ in ('true', 'Rejected')
    AND
    last_updated_time_ > '{START_TIME_STR}'
    AND
    last_updated_time_ <= '{END_TIME}'
"""


# get all proc_inst_id_ where credit_assessment_datetime date falls between START_TIME_STR and END_TIME.
GET_CREDIT_PROC_IDS_SQL = f"""
SELECT DISTINCT proc_inst_id_
FROM
    act_hi_varinst
WHERE
    name_ = 'credit_assessment_datetime'
    AND
    to_timestamp(text_, 'Dy Mon DD YYYY HH24:MI:SS') > TO_TIMESTAMP('{START_TIME_STR}','YYYY-MM-DD HH24:MI:SS.MS')
    AND
    to_timestamp(text_, 'Dy Mon DD YYYY HH24:MI:SS') <= TO_TIMESTAMP('{END_TIME}', 'YYYY-MM-DD HH24:MI:SS.MS')"""

# get all scope_id_ where credit_assessment_datetime date falls between START_TIME_STR and END_TIME.
GET_CREDIT_SCOPE_IDS_SQL = f"""
SELECT DISTINCT scope_id_
FROM
    act_hi_varinst
WHERE
    name_ = 'credit_assessment_datetime'
    AND
    to_timestamp(text_, 'Dy Mon DD YYYY HH24:MI:SS') > TO_TIMESTAMP('{START_TIME_STR}','YYYY-MM-DD HH24:MI:SS.MS')
    AND
    to_timestamp(text_, 'Dy Mon DD YYYY HH24:MI:SS') <= TO_TIMESTAMP('{END_TIME}', 'YYYY-MM-DD HH24:MI:SS.MS')"""


# get all scope_id_ where approveCIAppTimestamp date falls between START_TIME_STR and END_TIME.
GET_APPROVE_SCOPE_IDS_SQL = f"""
SELECT DISTINCT scope_id_
FROM
act_hi_varinst
WHERE
name_ = 'approveCIAppTimestamp'
AND
to_timestamp(text_, 'MM-DD-YYYY HH24:MI:SS.MS') > TO_TIMESTAMP('{START_TIME_STR}','YYYY-MM-DD HH24:MI:SS.MS')
AND
to_timestamp(text_, 'MM-DD-YYYY HH24:MI:SS.MS') <= TO_TIMESTAMP('{END_TIME}', 'YYYY-MM-DD HH24:MI:SS.MS')"""


# get all proc_inst_id_ where loan_creation_datetime date falls between START_TIME_STR and END_TIME.
GET_LOAN_PROC_IDS_SQL = f"""
SELECT DISTINCT proc_inst_id_
FROM
    act_hi_varinst
WHERE
    name_ = 'loan_creation_datetime'
    AND
    to_timestamp(text_, 'YYYY-MM-DD HH24:MI:SS.MS') > TO_TIMESTAMP('{START_TIME_STR}','YYYY-MM-DD HH24:MI:SS.MS')
    AND
    to_timestamp(text_, 'YYYY-MM-DD HH24:MI:SS.MS') <= TO_TIMESTAMP('{END_TIME}', 'YYYY-MM-DD HH24:MI:SS.MS')"""

# get all scope_id_ where loan_creation_datetime date falls between START_TIME_STR and END_TIME.
GET_LOAN_SCOPE_IDS_SQL = f"""
SELECT DISTINCT scope_id_
FROM
    act_hi_varinst
WHERE
    name_ = 'loan_creation_datetime'
    AND
    to_timestamp(text_, 'YYYY-MM-DD HH24:MI:SS.MS') > TO_TIMESTAMP('{START_TIME_STR}','YYYY-MM-DD HH24:MI:SS.MS')
    AND
    to_timestamp(text_, 'YYYY-MM-DD HH24:MI:SS.MS') <= TO_TIMESTAMP('{END_TIME}', 'YYYY-MM-DD HH24:MI:SS.MS')"""


# get all proc_inst_id_ where cancelTimestamp date falls between START_TIME_STR and END_TIME.
GET_CANCEL_PROC_IDS_SQL = f"""
SELECT DISTINCT proc_inst_id_
FROM
    act_hi_varinst
WHERE
    name_ = 'cancelTimestamp'
    AND
    to_timestamp(text_, 'MM-DD-YYYY HH24:MI:SS') > TO_TIMESTAMP('{START_TIME_STR}','YYYY-MM-DD HH24:MI:SS.MS')
    AND
    to_timestamp(text_, 'MM-DD-YYYY HH24:MI:SS') <= TO_TIMESTAMP('{END_TIME}', 'YYYY-MM-DD HH24:MI:SS.MS')"""

# get all scope_id_ where cancelTimestamp date falls between START_TIME_STR and END_TIME.
GET_CANCEL_SCOPE_IDS_SQL = f"""
SELECT DISTINCT scope_id_
FROM
    act_hi_varinst
WHERE
    name_ = 'cancelTimestamp'
    AND
    to_timestamp(text_, 'MM-DD-YYYY HH24:MI:SS') > TO_TIMESTAMP('{START_TIME_STR}','YYYY-MM-DD HH24:MI:SS.MS')
    AND
    to_timestamp(text_, 'MM-DD-YYYY HH24:MI:SS') <= TO_TIMESTAMP('{END_TIME}', 'YYYY-MM-DD HH24:MI:SS.MS')"""




def get_id_list(pg_cur):
    """Given postgres cursor, return list of ids.
    :param - pg_cur postgres cursor.
    """
    _id_list = []
    for row in pg_cur.fetchall():
        _id_list.append(row[0])
    return _id_list


def get_id_list_as_tuple(_id_list):
    """Return string tuple of list.
    :param - _id_list (list): list of IDs.
    """
    if len(_id_list) == 1:
        _id_str = f"('{_id_list[0]}')"
    elif len(_id_list) > 1:
        _id_str = f"{tuple(_id_list)}"
    else:
        _id_str = "('')"
    return _id_str


def convert_to_json(json_string):
    """Return json for given string content.
    :param - json_string(str): Valid Json String.
    """
    if json_string:
        try:
            json_string = json.loads(json_string)
        except Exception as e:
            print("Cant convert to json... returning empty json")
            json_string = dict()
    else:
        json_string = dict()
    return json_string


def getListOfValues(row, key, count):
    rowValue = None
    if key in row.index:
        rowValue = row[key]
    if len(rowValue) >= count:
        return rowValue[:count]
    else:
        rowValue.extend([None] * (count - len(rowValue)))
        return rowValue


def ftp_transfer(report_name):
    """ Transfer file to different server.
    :param report_name (str): name of the report name.
    """
    try:
        key = paramiko.RSAKey.from_private_key_file(EDMP_FTP_PRIVATE_KEY_FILE_PATH)
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=EDMP_FTP_HOST, username=EDMP_FTP_USERNAME, port=EDMP_FTP_PORT, pkey=key)
        sftp = ssh_client.open_sftp()
        sftp.put(report_name, EDMP_FTP_REMOTE_FILE_PATH + f"/{report_name.split('/')[-1]}")
        sftp.close()
        ssh_client.close()
    except Exception as e:
        print("ERROR: ERROR in connecting to remote server")
        print(e)


def create_edmp_bil_report(empty_report=False):
    """Create EDMP report. Merge proc_var df and portal_data df and add some other things to final df.
    :param empty_report (boolean): default False, if True creates empty reports with zero rows.
    """
    print("In creating edmp report....")
    edmp_bil_final_df = pd.DataFrame(columns=list(total_edmp_columns["db_name"]))
    if not empty_report:
        portal_bil_result_df.fillna('', inplace=True)
        result_var_df.fillna('', inplace=True)
        edmp_bil_final_df = pd.merge(result_var_df, portal_bil_result_df, on=['case_instance_id'], how='left')
    edmp_bil_final_df = edmp_bil_final_df[total_edmp_columns["db_name"]]
    edmp_bil_final_df.index.name = "Serial_Number"

    column_dict = dict(zip(total_edmp_columns['db_name'], total_edmp_columns['business_name']))
    edmp_bil_final_df.rename(columns=column_dict, inplace=True)

    today_date = datetime.today().strftime('%Y%m%d')
    if not os.path.exists('/scb-etl/edmp/edmp_reports'):
        os.makedirs('/scb-etl/edmp/edmp_reports')
    report_path = os.path.abspath('/scb-etl/edmp/edmp_reports')
    csv_file_name = os.path.join(report_path, f"SG_KUL_ACTHIVAR_BIL_{today_date}.csv")
    dat_file_name = os.path.join(report_path, f"SG_KUL_ACTHIVAR_BIL_{today_date}.dat")

    edmp_bil_final_df.to_csv(csv_file_name, sep=',', quotechar='"')

    edmp_bil_final_df['Proposed Loan amount'] = pd.to_numeric(edmp_bil_final_df['Proposed Loan amount'], errors='coerce')
    sum_of_loan_amount = int(edmp_bil_final_df['Proposed Loan amount'].sum())

    edmp_bil_dat_header = ["H", "1.1", "1.1", "SG", today_date, today_date, "1"]
    no_of_rows = edmp_bil_final_df.shape[0]
    loan_amount_index = edmp_bil_final_df.columns.get_loc('Proposed Loan amount') + 1
    no_of_checksum = 1
    edmp_bil_dat_trailer = ["T", f"{no_of_rows}", f"{no_of_checksum}", f"{loan_amount_index}", f"{sum_of_loan_amount}"]

    edmp_bil_final_df["DataIdentifier"] = "D"
    edmp_bil_final_df.set_index('DataIdentifier', inplace=True)

    with open(f"{dat_file_name}", "w") as m:
        m.write(chr(1).join(edmp_bil_dat_header) + "\n")  # writing the header to the file.
    edmp_bil_final_df.to_csv(f"{dat_file_name}", header=None, mode="a", index_label="D", sep=chr(1))  # appending data to the file
    with open(f"{dat_file_name}", "a") as m:
        m.write(chr(1).join(edmp_bil_dat_trailer))  # appending the trailer to the file
    print("EDMP report done.")
    return csv_file_name, dat_file_name


try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']} options='-c search_path={pg_cred['schema']}'")
    pg_cur = pg_conn.cursor()

    pg_cur.execute(GET_REJECTED_PROC_IDS_SQL)
    rejected_proc_id_list = get_id_list(pg_cur)
    print("Rejected proc_inst_id count:", len(rejected_proc_id_list))

    pg_cur.execute(GET_REJECTED_SCOPE_IDS_SQL)
    rejected_scope_id_list = get_id_list(pg_cur)
    print("Rejected scope_id count:", len(rejected_scope_id_list))

    pg_cur.execute(GET_CREDIT_PROC_IDS_SQL)
    credit_proc_id_list = get_id_list(pg_cur)
    print("Credit proc_inst_id count:", len(credit_proc_id_list))

    pg_cur.execute(GET_CREDIT_SCOPE_IDS_SQL)
    credit_scope_id_list = get_id_list(pg_cur)
    print("Credit scope_id count:", len(credit_scope_id_list))

    pg_cur.execute(GET_LOAN_PROC_IDS_SQL)
    loan_proc_id_list = get_id_list(pg_cur)
    print("Loan proc_inst_id count:", len(loan_proc_id_list))

    pg_cur.execute(GET_LOAN_SCOPE_IDS_SQL)
    loan_scope_id_list = get_id_list(pg_cur)
    print("Loan scope_id count:", len(loan_scope_id_list))

    pg_cur.execute(GET_CANCEL_PROC_IDS_SQL)
    cancel_proc_id_list = get_id_list(pg_cur)
    print("Cancel proc_inst_id count:", len(cancel_proc_id_list))

    pg_cur.execute(GET_CANCEL_SCOPE_IDS_SQL)
    cancel_scope_id_list = get_id_list(pg_cur)
    print("Cancel scope_id count:", len(cancel_scope_id_list))

    pg_cur.execute(GET_APPROVE_SCOPE_IDS_SQL)
    approve_scope_id_list = get_id_list(pg_cur)
    print("Approve scope_id count:", len(approve_scope_id_list))

    
    proc_id_list = list(set(rejected_proc_id_list + credit_proc_id_list + loan_proc_id_list + cancel_proc_id_list))
    scope_id_list = list(set(rejected_scope_id_list + credit_scope_id_list + loan_scope_id_list + cancel_scope_id_list + approve_scope_id_list))

    if None in proc_id_list:
        proc_id_list.remove(None)
    if None in scope_id_list:
        scope_id_list.remove(None)

    pg_conn.close()
except Exception as e:
    print(e)
    print("ERROR IN FETCHING ID LIST")
    error_message = f"ERROR IN FETCHING ID LIST: {e}"
    print(error_message)
    pg_conn.close()
    sys.exit(1)

# proc_id_list = []
# scope_id_list = []

if not any((proc_id_list, scope_id_list)):
    print("EXIT NOTHING TO PROCESS- Creating Empty report ...")
    # if there is not data, send empty report with all the headers
    edmp_csv_file_name, edmp_dat_file_name = create_edmp_bil_report(empty_report=True)
    ftp_transfer(edmp_dat_file_name)
    send_attachment_email([edmp_csv_file_name])

    update_etl_table_sql = f"""UPDATE sc_edmp_etl_logs SET status = 'SUCCESS' where id = {MAX_ID};"""
    try:
        pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']} options='-c search_path={pg_cred['schema']}'")
        pg_cur = pg_conn.cursor()
        pg_cur.execute(update_etl_table_sql)
        pg_conn.commit()
        pg_conn.close()
    except Exception as e:
        print(e)
        pg_conn.close()
        sys.exit(1)
    print("DONE")
    sys.exit(1)

proc_id_list_str = get_id_list_as_tuple(proc_id_list)
scope_id_list_str = get_id_list_as_tuple(scope_id_list)
case_instance_id_list = scope_id_list.copy()

case_instance_id_str = get_id_list_as_tuple(case_instance_id_list)

print("Final proc_inst_id count:", len(set(proc_id_list)))
print("Final scope_id_ count:", len(set(scope_id_list)))
print(f"Final case_instance_id count: {len(case_instance_id_list)}")

# this query gets data from act_hi_varinst for all given proc_inst_id.
GET_VAR_DATA_SQL = f"""SELECT
    PROC_VAR.PROC_INST_ID_ AS PROC_VAR_PROC_INST_ID_ ,
    PROC_VAR.PROCESS_VARIABLE AS PROC_VAR_PROCESS_VARIABLE
FROM
    (
        SELECT
            PROC_INST_ID_,
            (json_object_agg(key, value)) AS PROCESS_VARIABLE
        FROM
            (
                SELECT
                   PROC_INST_ID_,
                   (
                        CASE
                            WHEN VAR_TYPE_ IN ('string')
                                THEN  json_build_object(NAME_, TEXT_)
                            WHEN VAR_TYPE_ IN ('long' , 'integer', 'boolean')
                                 THEN json_build_object(NAME_, LONG_)
                            WHEN VAR_TYPE_ = 'double'
                                 THEN json_build_object(NAME_, DOUBLE_)
                            WHEN VAR_TYPE_ IN ('serializable', 'longString')
                                THEN json_build_object(NAME_, (select encode(bytes_::bytea, 'escape') from act_ge_bytearray where id_ = BYTEARRAY_ID_))
                        END
                    ) AS json_data
                FROM
                    ACT_HI_VARINST
            ) as A, json_each(json_data)  where PROC_INST_ID_ in {proc_id_list_str}
        GROUP BY
            PROC_INST_ID_
    ) PROC_VAR ;"""


# this query gets data from act_hi_varinst for all given scope_id_.
GET_SCOPE_DATA_SQL = f"""SELECT
    SCOPE_VAR.SCOPE_ID_ AS PROC_VAR_PROC_INST_ID_ ,
    SCOPE_VAR.PROCESS_VARIABLE AS PROC_VAR_PROCESS_VARIABLE
FROM
    (
        SELECT
            SCOPE_ID_,
            (json_object_agg(key, value)) AS PROCESS_VARIABLE
        FROM
            (
                SELECT
                   SCOPE_ID_,
                   (
                        CASE
                            WHEN VAR_TYPE_ IN ('string')
                                THEN  json_build_object(NAME_, TEXT_)
                            WHEN VAR_TYPE_ IN ('long' , 'integer', 'boolean')
                                 THEN json_build_object(NAME_, LONG_)
                            WHEN VAR_TYPE_ = 'double'
                                 THEN json_build_object(NAME_, DOUBLE_)
                            WHEN VAR_TYPE_ IN ('serializable', 'longString')
                                THEN json_build_object(NAME_, (select encode(bytes_::bytea, 'escape') from act_ge_bytearray where id_ = BYTEARRAY_ID_))
                        END
                    ) AS json_data
                FROM
                    ACT_HI_VARINST
            ) as A, json_each(json_data)  where SCOPE_ID_ in {scope_id_list_str}
        GROUP BY
            SCOPE_ID_
    ) SCOPE_VAR ;"""

# this query gets data from portal_audit for all given case_instance_id.
GET_PORTAL_DATA = f"""
                  SELECT row_to_json(T) FROM
                  (
                  SELECT
                    a.modified AS  portal_audit_modified,
                    a.attributes AS  portal_audit_attributes,
                    a.case_instance_id AS  portal_audit_case_instance_id,
                    a.intermediate_request_body AS  portal_audit_intermediate_request_body,
                    a.intermediate_response_body AS  portal_audit_intermediate_response_body,
                    a.new_process_variables AS  portal_audit_new_process_variables,
                    a.old_process_variables AS  portal_audit_old_process_variables,
                    a.parameters AS  portal_audit_parameters,
                    a.request_body AS  portal_audit_request_body,
                    a.request_id AS  portal_audit_request_id,
                    a.requesturi AS  portal_audit_requesturi,
                    a.response_body AS  portal_audit_response_body,
                    a.role_name AS  portal_audit_role_name,
                    a.status AS  portal_audit_status,
                    a.tab_key AS  portal_audit_tab_key,
                    a.user_id AS  portal_audit_user_id,
                    a.mapper_id AS  portal_audit_mapper_id,
                    a.portal_comment_history_id AS  portal_audit_portal_comment_history_id,
                    a.portal_user_id  AS  portal_audit_portal_user_id

                  FROM portal_audit a
                  where a.case_instance_id in {case_instance_id_str}
                  ORDER BY a.modified ASC
                  ) T ;"""

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']} options='-c search_path={pg_cred['schema']}'")
    pg_cur = pg_conn.cursor()

    if not proc_id_list_str:
        varinst_data = []
    else:
        pg_cur.execute(GET_VAR_DATA_SQL)
        varinst_data = pg_cur.fetchall()

    if not scope_id_list_str:
        scope_varinst_data = []
    else:
        pg_cur.execute(GET_SCOPE_DATA_SQL)
        scope_varinst_data = pg_cur.fetchall()

    if not case_instance_id_str:
        portal_data = []
    else:
        pg_cur.execute(GET_PORTAL_DATA)
        portal_data = pg_cur.fetchall()
        portal_data = [data[0] for data in portal_data]

    pg_conn.close()
except Exception as e:
    print(e)
    print("ERROR IN FETCHING DATA")
    error_message = f"ERROR IN FETCHING DATA: {e}"
    error_capture(conn, cur, status='FAILED', error_message=error_message)
    pg_conn.close()
    sys.exit(1)


# dump proc varinst data in Dataframe.
def insert_proc_var_data():
    final_proc_var_df = pd.DataFrame(columns=BIL_PROC_VARS + ['proc_inst_id'])
    try:
        for i, big_var_data in enumerate(varinst_data):
            proc_inst_id = big_var_data[0]
            var_data = big_var_data[1]
            var_data["proc_inst_id"] = proc_inst_id

            proc_var_df = pd.DataFrame([var_data], columns=BIL_PROC_VARS + ['proc_inst_id'])
            final_proc_var_df = final_proc_var_df.append(proc_var_df)
        return final_proc_var_df

    except Exception as e:
        print(e)
        error_message = f"ERROR WHILE INSERTING PROC VAR DATA: {e}"
        print(error_message)
        return False


# dump scope varinst data in Dataframe.
def insert_scope_var_data():
    final_scope_var_df = pd.DataFrame(columns=BIL_PROC_VARS + ['scope_id'])
    try:
        for i, big_var_data in enumerate(scope_varinst_data):
            scope_id = big_var_data[0]
            var_data = big_var_data[1]
            var_data["scope_id"] = scope_id

            proc_var_df = pd.DataFrame([var_data], columns=BIL_PROC_VARS + ['scope_id'])
            final_scope_var_df = final_scope_var_df.append(proc_var_df)
        return final_scope_var_df

    except Exception as e:
        print(e)
        error_message = f"ERROR WHILE INSERTING PROC VAR DATA: {e}"
        print(error_message)
        return False


# dump portal data in DataFrame.
def insert_portal_data():
    print("IN INSERT PORTAL DATA")
    bil_portal_column_list = ["case_instance_id"]

    for key in BIL_PORTAL_COLUMNS:
        bil_portal_column_list.append(key + "_timestamp")
        bil_portal_column_list.append(key + "_message")

    final_bil_result_df = pd.DataFrame(columns=bil_portal_column_list)

    for i, data in enumerate(portal_data):
        request_body = data.get("portal_audit_request_body", None)
        if request_body is None or request_body.strip() == "":
            continue

        modified_time = data.get("portal_audit_modified", None)
        case_instance_id = data.get("portal_audit_case_instance_id", None)
        role_name = data.get("portal_audit_role_name", None)

        data_df = pd.DataFrame(columns=bil_portal_column_list)
        try:
            request_body = json.loads(request_body)
        except json.decoder.JSONDecodeError:
            print("Malformed request_body")
            continue

        if request_body and isinstance(request_body, dict):
            action_key = request_body.get("key", None)
            message = request_body.get("message", None)
            business_key = None
            if role_name == "CA" and action_key == 'submitCA':
                frm_review_flag = request_body.get("isFrmReview", None)
                bucket_key = request_body.get("bucketKey", None)
                if frm_review_flag is None or bucket_key is None:
                    nested_req_body = request_body.get("requestBody", None)
                    if nested_req_body and isinstance(nested_req_body, dict):
                        if frm_review_flag is None:
                            frm_review_flag = nested_req_body.get("isFrmReview", None)
                        if bucket_key is None:
                            bucket_key = nested_req_body.get("bucketKey", None)

                if frm_review_flag is not None and (frm_review_flag in ('true', 'True', True)):
                    business_key = "Referred by CI Analyst to FRM"
                else:
                    business_key = "Referred by CI Analyst to Approver"

                if bucket_key is not None and bucket_key == 'phoneDiscussion':
                    business_key = "phoneDiscussionCompleted"
                elif bucket_key is not None and bucket_key == 'siteVisit':
                    business_key = "site_visit_Completed"

            elif role_name in ("CI_ApproverL1", "CI_ApproverL2", "CI_ApproverL3") and action_key == 'approveCIApp':
                business_key = ["Approved by CI Approver", "Referred by CI Approver to Sales"]
            elif role_name == 'Sales_Agent' and action_key == 'submitCustAccpt':
                # proc_id = proc_case_zip[case_instance_id]
                if not result_var_df[result_var_df['case_instance_id'] == case_instance_id]['productType'].empty:
                    productType = result_var_df[result_var_df['case_instance_id'] == case_instance_id]['productType'].values[0]
                    updatedloaninterest = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['updatedloaninterest'].values[0], errors='coerce')
                    creditgivenloanamount = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['creditgivenloanamount'].values[0], errors='coerce')
                    updatednewloanamount = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['updatednewloanamount'].values[0], errors='coerce')
                    Interest_Rate_BIL = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['Interest_Rate_BIL'].values[0], errors='coerce')
                    Interest_Rate_WCL = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['Interest_Rate_WCL'].values[0], errors='coerce')
                    Interest_Rate_TBL = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['Interest_Rate_TBL'].values[0], errors='coerce')

                    if productType == 'BIL':

                        if((updatedloaninterest >= Interest_Rate_BIL) and (creditgivenloanamount >= updatednewloanamount)):
                            business_key = ["Referred by Sales by Ops", "Customer_Acceptance"]
                        elif creditgivenloanamount < updatednewloanamount:
                            business_key = "Referred by Sales to CI Analyst"
                        else:
                            business_key = "Referred by Sales to Sales Head"
                    elif productType == 'WCL':
                        if((updatedloaninterest >= Interest_Rate_WCL) and (creditgivenloanamount >= updatednewloanamount)):
                            business_key = ["Referred by Sales by Ops", "Customer_Acceptance"]
                        elif creditgivenloanamount < updatednewloanamount:
                            business_key = "Referred by Sales to CI Analyst"
                        else:
                            business_key = "Referred by Sales to Sales Head"
                    elif productType == 'TBL':
                        if((updatedloaninterest >= Interest_Rate_TBL) and (creditgivenloanamount >= updatednewloanamount)):
                            business_key = ["Referred by Sales by Ops", "Customer_Acceptance"]
                        elif creditgivenloanamount < updatednewloanamount:
                            business_key = "Referred by Sales to CI Analyst"
                        else:
                            business_key = "Referred by Sales to Sales Head"
                    else:
                        updatedloaninterestBil = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['updatedloaninterestBil'].values[0], errors='coerce')
                        updatedloaninterestWcl = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['updatedloaninterestWcl'].values[0], errors='coerce')
                        updatednewloanamountbundle = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['updatednewloanamountbundle'].values[0], errors='coerce')
                        updatedloaninterestTbl = pd.to_numeric(result_var_df[result_var_df['case_instance_id'] == case_instance_id]['updatedloaninterestTbl'].values[0], errors='coerce')

                        if(((updatedloaninterestBil >= Interest_Rate_BIL) or (updatedloaninterestWcl >= Interest_Rate_WCL) or (updatedloaninterestTbl >= Interest_Rate_TBL)) and (creditgivenloanamount >= updatednewloanamountbundle)):
                            business_key = ["Referred by Sales by Ops", "Customer_Acceptance"]
                        elif creditgivenloanamount < updatednewloanamountbundle:
                            business_key = "Referred by Sales to CI Analyst"
                        else:
                            business_key = "Referred by Sales to Sales Head"
            else:
                if role_name and action_key:
                    business_key = EDMP_BIL_ACTION_MAP.loc[(EDMP_BIL_ACTION_MAP.action_key.str.lower() == action_key.lower()) & (EDMP_BIL_ACTION_MAP.role_id.str.lower() == role_name.lower())]["action_name_ui"].values
                if business_key:
                    business_key = business_key[0]

            if not isinstance(business_key, list):
                business_key = [business_key]
            if business_key:
                for key in business_key:
                    if key in BIL_PORTAL_COLUMNS:
                        if case_instance_id in final_bil_result_df['case_instance_id'].values:
                            key_timestamp = str(final_bil_result_df.loc[final_bil_result_df.case_instance_id == case_instance_id, key + "_timestamp"].values[0])

                            if key_timestamp == 'nan' or key_timestamp <= modified_time:
                                final_bil_result_df.loc[final_bil_result_df.case_instance_id == case_instance_id, key + "_timestamp"] = modified_time
                                final_bil_result_df.loc[final_bil_result_df.case_instance_id == case_instance_id, key + "_message"] = message
                        else:
                            # print(proc_case_zip[case_instance_id])
                            # data_df.at[0, "proc_inst_id"] = proc_case_zip[case_instance_id]
                            data_df.at[0, "case_instance_id"] = case_instance_id
                            data_df.at[0, key + "_timestamp"] = modified_time
                            data_df.at[0, key + "_message"] = message
                            final_bil_result_df = final_bil_result_df.append(data_df, ignore_index=True)
            else:
                pass
    final_bil_result_df = final_bil_result_df[bil_portal_column_list]
    return final_bil_result_df


try:
    bil_proc_df = insert_proc_var_data()
    # get only those results where journeyName is bilUserJourney
    bil_proc_df = bil_proc_df[bil_proc_df['journeyName'].isin(['bilUserJourney','bilPaperJourney'])]

    bil_scope_df = insert_scope_var_data()
    # get only those results where journeyName is bilUserJourney
    bil_scope_df = bil_scope_df[bil_scope_df['journeyName'].isin(['bilUserJourney','bilPaperJourney'])]

    bil_scope_df = bil_scope_df.astype({'applicationId': str})
    bil_proc_df = bil_proc_df.astype({'applicationId': str})

    print(f"\nTotal Updated proc_inst_id for bilUserJourney: {bil_proc_df.shape[0]}")
    print(f"Total Updated scope_id for bilUserJourney: {bil_scope_df.shape[0]}\n")
except Exception as e:
    print(e)
    print("ERROR IN CONNECTING TARGET DB")

proc_df_rows = bil_proc_df.shape[0]
scope_df_rows = bil_scope_df.shape[0]

bil_scope_df = bil_scope_df.set_index('applicationId')
bil_scope_df = bil_scope_df[~bil_scope_df.index.duplicated(keep="first")]
bil_proc_df = bil_proc_df.set_index('applicationId')
bil_proc_df = bil_proc_df[~bil_proc_df.index.duplicated(keep="first")]

# the following if-else make sure that, we are giving first preference to scope_id_ data to be considered as final.
if scope_df_rows >= proc_df_rows:
    print("scope rows are >= proc rows")
    result_var_df = bil_scope_df.reindex(columns=bil_scope_df.columns.union(bil_proc_df.columns))
    result_var_df.update(bil_proc_df, overwrite=False)
    result_var_df.reset_index(inplace=True)
else:
    print("scope rows are < proc rows")
    result_var_df = bil_proc_df.reindex(columns=bil_proc_df.columns.union(bil_scope_df.columns))
    result_var_df.update(bil_scope_df, overwrite=True)
    result_var_df.reset_index(inplace=True)

result_var_df['loanAmount_system_bil'].fillna(0, inplace=True)
result_var_df['proc_inst_id'].fillna('', inplace=True)
result_var_df['scope_id'].fillna('', inplace=True)

print("Empty values for result var df filled with none.")

# proc_case_df = pd.DataFrame(list(proc_case_zip.items()), columns=['case_instance_id', 'proc_inst_id'])
# proc_case_df['case_instance_id'].fillna('', inplace=True)
# proc_case_df['proc_inst_id'].fillna('', inplace=True)

result_var_df.rename(columns={"scope_id": "case_instance_id"}, inplace=True)
# result_var_df = pd.merge(result_var_df, proc_case_df, left_on=['proc_inst_id', 'case_instance_id'], right_on=['proc_inst_id', 'case_instance_id'], how='left')

portal_bil_result_df = insert_portal_data()
print("Insert portal data Done")

""" below logic goes like this:
Accepted Interest Rate BIL (136) -
    When productType is BIL, use updatedloaninterest,
    when productType is BUNDLE use updatedloaninterestBil.
    In case productType is BIL, then dont update row 137
Accepted Interest Rate WCL -
    When productType is WCL, use updatedloaninterest,
    When productType is BUNDLE use updatedloaninterestWcl.
    In case productType is WCL, then dont update row 136
"""

disbursement_existing_acc_boolean = {"setupCurrentAccountNo": "Yes", "setCurrentAccountYes": "No"}

facilityType_user_bil_replace_dict = dict({
    "businessCreditCard_user_bil": "Business Credit Card",
    "hirePurchase_user_bil": "Hire Purchase",
    "overDraft_user_bil": "Overdraft",
    "propertyLoan_user_bil": "Property Loan",
    "termLoan_user_bil": "Term Loan",
    "workingCapitalLoan_user_bil": "Enterprise Singapore SME Working Capital Loan",
    "trade_user_bil": "Trade",
    "otherFacilityType_user_bil": "Others"
})

purposeOfLoan_user_bil_replace_dict = dict({
    "forWorkingCapital_user_bil": "For Working Capital",
    "forImportExportOfGoods_user_bil": "For import/export of Goods",
    "forBuyingOfEquipmentMachineryVehicles_user_bil": "For buying of equipment/machinery/vehicles",
    "forRenovationOfBusinessPremises_user_bil": "For renovation of business",
    "otherPurposeOfTakingLoan": "Others"
})

result_var_df.fillna('', inplace=True)
result_var_df.replace({"setupLocalCurrentAccount": disbursement_existing_acc_boolean}, inplace=True)
result_var_df.replace({"purposeOfLoan_user_bil": purposeOfLoan_user_bil_replace_dict}, inplace=True)

result_var_df.loc[result_var_df['productType'] == 'BIL', 'updatedloaninterestBil'] = result_var_df[result_var_df['productType'] == 'BIL']['updatedloaninterest']
result_var_df.loc[result_var_df['productType'] == 'BUNDLE', 'updatedloaninterestBil'] = result_var_df[result_var_df['productType'] == 'BUNDLE']['updatedloaninterestBil']
result_var_df.loc[result_var_df['productType'] == 'BIL', 'updatedloaninterestWcl'] = None
result_var_df.loc[result_var_df['productType'] == 'BIL', 'Interest_Rate_WCL'] = None
result_var_df.loc[result_var_df['productType'] == 'BIL', 'updatedloaninterestTbl'] = None
result_var_df.loc[result_var_df['productType'] == 'BIL', 'Interest_Rate_TBL'] = None
result_var_df.loc[result_var_df['productType'] == 'WCL', 'updatedloaninterestWcl'] = result_var_df[result_var_df['productType'] == 'WCL']['updatedloaninterest']
result_var_df.loc[result_var_df['productType'] == 'TBL', 'updatedloaninterestTbl'] = result_var_df[result_var_df['productType'] == 'TBL']['updatedloaninterest']
result_var_df.loc[result_var_df['productType'] == 'BUNDLE', 'updatedloaninterestWcl'] = result_var_df[result_var_df['productType'] == 'BUNDLE']['updatedloaninterestWcl']
result_var_df.loc[result_var_df['productType'] == 'WCL', 'updatedloaninterestBil'] = None
result_var_df.loc[result_var_df['productType'] == 'WCL', 'Interest_Rate_BIL'] = None
result_var_df.loc[result_var_df['productType'] == 'WCL', 'updatedloaninterestTbl'] = None
result_var_df.loc[result_var_df['productType'] == 'WCL', 'Interest_Rate_TBL'] = None
result_var_df.loc[result_var_df['productType'] == 'TBL', 'updatedloaninterestWcl'] = None
result_var_df.loc[result_var_df['productType'] == 'TBL', 'Interest_Rate_WCL'] = None
result_var_df.loc[result_var_df['productType'] == 'TBL', 'updatedloaninterestBil'] = None
result_var_df.loc[result_var_df['productType'] == 'TBL', 'Interest_Rate_BIL'] = None

result_var_df.loc[result_var_df['productType'].isin(['WCL', 'BIL', 'TBL']), 'updatednewloanamount_condition'] = result_var_df.loc[result_var_df['productType'].isin(['WCL', 'BIL', 'TBL']), 'updatednewloanamount']
result_var_df.loc[~result_var_df['productType'].isin(['WCL', 'BIL', 'TBL']), 'updatednewloanamount_condition'] = result_var_df.loc[~result_var_df['productType'].isin(['WCL', 'BIL', 'TBL']), 'updatednewloanamountbundle']


result_var_df.loc[result_var_df['userType_sys_bil'] == 'user', 'userType_sys_bil_condition'] = 'End User' 
result_var_df.loc[(result_var_df['userType_sys_bil'] == '') & ((result_var_df['guarantorMyInfoDetailKyc'] != '') | (result_var_df['journeyName'] == 'bilPaperJourney')) ,'userType_sys_bil_condition'] = 'Paper Journey' 
result_var_df.loc[(result_var_df['userType_sys_bil'] == '') & (result_var_df['journeyName'] != 'bilPaperJourney') & (result_var_df['guarantorMyInfoDetailKyc'] == ''),'userType_sys_bil_condition'] = 'RM Assisted Journey'

print("Data replacement on productType is done")

for key in shareholderDetailsTableHeading_details['keys']:
    result_var_df.shareholderDetailsTableHeading = result_var_df.shareholderDetailsTableHeading.map(lambda x: x[x.find('['):])
    result_var_df[key] = result_var_df.shareholderDetailsTableHeading.map(lambda x: [i.get(key, None) for i in convert_to_json(x)])
    count = shareholderDetailsTableHeading_details["count"]
    key_list = [f"{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

for key in litigationCompany_details['keys']:
    result_var_df[key] = result_var_df.litigationCompany.map(lambda x: [i.get(key, None) for i in convert_to_json(x)])
    count = litigationCompany_details["count"]
    key_list = [f"litigation_{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

for key in litigationIndividualData_details['keys']:
    result_var_df[key] = result_var_df.litigationIndividualData.map(lambda x: [i.get(key, None) for i in convert_to_json(x)])
    count = litigationIndividualData_details["count"]
    key_list = [f"litigationInfo_{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

for key in financialCommitments_user_bil_details['keys']:
    result_var_df[key] = result_var_df.financialCommitments_user_bil.map(lambda x: [i.get(key, None) for i in convert_to_json(x)])
    count = financialCommitments_user_bil_details["count"]
    key_list = [f"{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    if key == 'facilityType_user_bil':
        for keyType in key_list:
            result_var_df.replace({keyType: facilityType_user_bil_replace_dict}, inplace=True)
    result_var_df.drop(columns=[key], inplace=True)

for key in guarantorMyInfoData_details['keys']:
    result_var_df[key] = result_var_df.guarantorInfo_system_bil.map(lambda x: [i.get(key, None) for i in convert_to_json(x)])
    count = guarantorMyInfoData_details["count"]
    key_list = [f"{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

for key in Offs_Director_System_bil_details["keys"]:
    result_var_df[f"{key}_dir"] = result_var_df.Offs_Director_System_bil.map(lambda x: [i.get(key, None) for i in convert_to_json(x)])
    count = Offs_Director_System_bil_details["count"]
    key_list = [f"{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, f"{key}_dir", count), axis=1, result_type='expand')
    result_var_df.drop(columns=[f"{key}_dir"], inplace=True)

for key in etbNtb_icm_system_details["keys"]:
    result_var_df[key] = result_var_df.etbNtb_icm_system.map(lambda x: [i.get(key, None) for i in convert_to_json(x)])
    count = etbNtb_icm_system_details["count"]
    key_list = [f"{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

for i in range(1, 6):
    for key in guarantorDetails_details['keys']:
        result_var_df[f'guarantorDetails{i}'] = result_var_df[f'guarantorDetails{i}'].map(lambda x: x[x.find('{'):])
        result_var_df[f"guarantorDetails{i}"] = result_var_df[f"guarantorDetails{i}"].str.replace(r"\\\\", r'\\')

        result_var_df[key + str(i)] = result_var_df[f'guarantorDetails{i}'].map(lambda x: convert_to_json(x).get(key, None))

for i, key in enumerate(guarantorRiskCodes_details['outer_keys']):
    result_var_df[key] = result_var_df.guarantorRiskCodes.map(lambda x: [json_dict.values() for index, json_dict in enumerate(convert_to_json(x)) if i==index])
    result_var_df[key] = result_var_df[key].map(lambda x: [value for i in x for value in i])  # flatten the list
    result_var_df[key] = result_var_df[key].map(lambda x: [value for i in x for value in i])  # flatten the list
    # result_var_df[key] = result_var_df[key].map(lambda x: [value for value in x if value])  # removing None values from the list
    count = guarantorRiskCodes_details["count"]
    key_list = [f"{key}_{guarantorRiskCodes_details['inner_key']}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

for i, key in enumerate(companyRiskCode_details['keys']):
    result_var_df[key] = result_var_df.companyRiskCode.map(lambda x: [i for i in convert_to_json(x)])
    count = companyRiskCode_details["count"]
    key_list = [f"{key}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

for i, key in enumerate(relatedPartyRiskCode_details['outer_keys']):
    result_var_df[key] = result_var_df.relatedPartyRiskCode.map(lambda x: [json_dict.values() for index, json_dict in enumerate(convert_to_json(x)) if i==index])
    result_var_df[key] = result_var_df[key].map(lambda x: [value for i in x for value in i])  # flatten the list
    result_var_df[key] = result_var_df[key].map(lambda x: [value for i in x for value in i])  # flatten the list
    # result_var_df[key] = result_var_df[key].map(lambda x: [value for value in x if value])  # removing None values from the list
    count = relatedPartyRiskCode_details["count"]
    key_list = [f"{key}_{relatedPartyRiskCode_details['inner_key']}{i}" for i in range(1, count + 1)]
    result_var_df[key_list] = result_var_df.apply(lambda row: getListOfValues(row, key, count), axis=1, result_type='expand')
    result_var_df.drop(columns=[key], inplace=True)

result_var_df.drop(columns=BIL_JSON_FIELDS, inplace=True)
print("Json fields transformation done.")

edmp_csv_file_name, edmp_dat_file_name = create_edmp_bil_report()

ftp_transfer(edmp_dat_file_name)
print("FTP Transfer to remote server done.")
send_attachment_email([edmp_csv_file_name])
print("Report email sent done.")

update_etl_table_sql = f"""UPDATE sc_edmp_etl_logs SET status = 'SUCCESS' where id = {MAX_ID};"""

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']} options='-c search_path={pg_cred['schema']}'")
    pg_cur = pg_conn.cursor()
    pg_cur.execute(update_etl_table_sql)
    pg_conn.commit()
    pg_conn.close()
except Exception as e:
    print(e)
    pg_conn.close()
    sys.exit(1)
